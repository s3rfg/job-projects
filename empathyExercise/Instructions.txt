
The project has been generated using Eclipse and Gradle, both the Eclipse project and Gradle build files have been included to make easier importing the project and its dependencies.
The front part is contained in frontExercise folder inside /empathyExercise.
The backend part was constructed using Java + SpringBoot and the frontend using Html5 + CSS + AngularJS + Bootstrap.

The application uses default setting to set up a tomcat web server in the port 8080 of localhost, the GET call is directed against /countries and the rest of requests against /country or /country/{id}
Dont go crazy trying to test it as the preloaded Countries in the Map structure are showing in /countries but I couldn`'t manage to show them of the front.