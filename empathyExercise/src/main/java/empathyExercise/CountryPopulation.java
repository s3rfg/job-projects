package empathyExercise;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.assertj.core.util.Maps;
import org.springframework.stereotype.Repository;

@Repository
public class CountryPopulation {
    private static final Map<Integer, Country> countries = new HashMap<Integer, Country>();

public CountryPopulation(){
	setInitialCountries();
}



/**
 * Generates an ID for every new Country taking into consideration if there are any free spots between existing ones
 * 
 * @return The generated id
 */
private int idGenerator() {
	int idGen = 0;
	for (int i = 0; i < countries.size(); i++) {
		if (!countries.containsValue(idGen))
			return idGen;
		idGen++;
	}
	return idGen;
}

public List<Country> getCountries(){
	List<Country> allCountries= new ArrayList<Country>(countries.values());
	return allCountries;
}

public Country getCountry(int id) {

	return countries.get(id);
}

public Country addCountry(Country newCountry) {
	newCountry.setId(idGenerator());
	countries.put(newCountry.getId(), newCountry);
	return newCountry;

		
}

public Country editCountry(Country editedCountry) {
	Country auxCountry = this.getCountry(editedCountry.getId());
	if(auxCountry != null) {
		auxCountry.setName(editedCountry.getName());
		auxCountry.setPopulation(editedCountry.getPopulation());
	}
	return auxCountry;
}

public void deleteCountry(int id) {
	countries.remove(id);
}

private void setInitialCountries() {
	  Country one = new Country(1, "AAA", 1);
	  Country two = new Country(2, "BBB", 2);
	  Country three = new Country(3, "CCC", 2);
	  
	  countries.put(one.getId(), one);
	  countries.put(two.getId(), two);
	  countries.put(three.getId(), three);
	  
}


}                                       