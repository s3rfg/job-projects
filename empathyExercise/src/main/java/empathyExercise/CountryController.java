package empathyExercise;

import java.awt.PageAttributes.MediaType;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CountryController {
	
	@Autowired
	CountryPopulation dao = new CountryPopulation();
	
	 @GetMapping(path="/countries", produces="application/json")
	 public List<Country> getCountries() {
	    return dao.getCountries();
	  }

	  @PostMapping(path = "/country", consumes = "application/json", produces="application/json")
	 public Country addCountry(@RequestBody Country countryToAdd) {
	    return dao.addCountry(countryToAdd);
	  }

	  // Single item

	  @GetMapping("/country/{id}")
	  public Country singleCountry(@PathVariable int id) {

	    return dao.getCountry(id);
	  }

	  @PutMapping(path = "/country/{id}", consumes = "application/json", produces = "application/json")
	 public Country editCountry(@RequestBody Country countryToEdit, @PathVariable int id) {

	    return dao.editCountry(countryToEdit);
	  }

	  @DeleteMapping("/country/{id}")
	 public void deleteEmployee(@PathVariable int id) {
	    dao.deleteCountry(id);
	  }
	  


}


