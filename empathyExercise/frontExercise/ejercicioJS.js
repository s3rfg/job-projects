var app = angular.module("CountryJS", []);
 
// Controller Part
app.controller("CountryController", function($scope, $http) {
 
 
    $scope.countries = [];
    $scope.country = {
        countryId: 1,
        countryName: "",
        countryPopulation: ""
    };


    //Load the data from back
   refreshCountryData();
 
    // HTTP POST/PUT methods for add/edit country  
    // Call: http://localhost:8080/country
    $scope.submitCountry = function() {
 
        var method = "";
        var url = "";
 
        if ($scope.country.countryId == -1) {
            method = "POST";
            url = '/country';
        } else {
            method = "PUT";
            url = '/country';
        }
 
        $http({
            method: method,
            url: url,
            data: angular.toJson($scope.country),
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(success, error);
    };
 
    $scope.createCountry = function() {
        clearFormData();
    }
 
    // HTTP DELETE- delete country by Id
    // Call: http://localhost:8080/country/{countryId}
    $scope.deleteCountry = function(country) {
        $http({
            method: 'DELETE',
            url: '/country/' + country.countryId
        }).then(success, error);
    };
 
    // In case of edit
    $scope.editCountry = function(country) {
        $scope.country.countryId = country.countryId;
        $scope.country.countryName = country.countryName;
        $scope.country.countryPopulation = country.countryPopulation;
    };
 
    // Private Method  
    // HTTP GET- get all countries collection
    // Call: http://localhost:8080/countries
    function refreshCountryData() {
        $http({
            method: 'GET',
            url: 'http://localhost:8080/countries'
        }).then(
            function(res) { // success
                $scope.countries = res.data;
            },
            function(res) { // error
                console.log("Error: " + res.status + " : " + res.data);
            }
        );
    }
 
    function success(res) {
        refreshCountryData();
        clearFormData();
    }
 
    function error(res) {
        var data = res.data;
        var status = res.status;
        var header = res.header;
        var config = res.config;
        alert("Error: " + status + ":" + data);
    }
 
    // Clear the form
    function clearFormData() {
        $scope.country.countryId = -1;
        $scope.country.countryName = "";
        $scope.country.countryPopulation = ""
    };
});